use cache::SvgCache;
use http_response::HttpResponse;
use regex::Regex;
use settings::Settings;
use std::io;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::time::Instant;
use svg::Page;

mod cache;
mod constants;
mod http_response;
mod settings;
mod svg;

fn main() -> io::Result<()> {
    let settings = Settings::new();
    let cache_instance = SvgCache::connect_or_create(&settings);
    let listener = TcpListener::bind(settings.socket()).unwrap();

    println!(" ");
    println!("Listening for requests on http://{}", settings.socket());
    println!(" ");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream, &cache_instance, &settings);
    }
    Ok(())
}

#[allow(clippy::unused_io_amount)]
fn handle_connection(mut stream: TcpStream, cache_instance: &SvgCache, settings: &Settings) {
    let now = Instant::now();
    print!("{}: ", stream.peer_addr().unwrap());
    let mut response = HttpResponse::new();
    match parse_request(&stream) {
        Some((volume, page)) => {
            let mut svg = Page::new(volume, page, settings, cache_instance);
            if let Some(data) = svg.get_data_from_cache() {
                svg.data = data;
                print!("from cache | ");
            } else {
                print!("rendering | ");
                svg.slowly_render_from_data();
                print!("caching | ");
                svg.add_new_data_to_cache();
            }
            println!(
                "serving {}-{}.svg | {} ms",
                volume,
                page,
                now.elapsed().as_millis()
            );
            response.status_code = 200;
            response.data = svg.data;
            stream.write(&response.header()).unwrap();
        }
        None => {
            println!("{} ms", now.elapsed().as_millis());
            response.status_code = 404;
            stream.write(&response.header()).unwrap();
        }
    };
    stream.write(&response.body()).unwrap();
    stream.flush().unwrap();
}

#[allow(clippy::unused_io_amount)]
fn parse_request(mut stream: &TcpStream) -> Option<(usize, usize)> {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();
    let regex = Regex::new(r"^GET /([1-9][0-9]{0,5}|1000000)-([1-9]|[1-9][0-9]|[1-2][0-9][0-9]|3[0-8][0-9]|39[0-1])\.svg ")
        .unwrap();
    let start_of_req = std::str::from_utf8(&buffer[0..21]).unwrap();
    let regex_capture = regex.captures(start_of_req);
    print!("{}… | ", start_of_req);

    match regex_capture {
        Some(cap) => Some((
            cap[1].parse::<usize>().unwrap(),
            cap[2].parse::<usize>().unwrap(),
        )),
        None => None,
    }
}

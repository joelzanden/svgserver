use super::cache::SvgCache;
use super::constants::numbers::*;
use super::constants::strings::*;
use super::settings::Settings;
use std::fs::File;
use std::io::prelude::*;
use std::io::SeekFrom;
use std::str;
use thousands::Separable;

#[derive(Debug)]
pub struct Page<'a> {
    settings: &'a Settings,
    cache_instance: &'a SvgCache,
    pub found_in_cache: bool,
    pub volume: usize,
    pub page: usize,
    show_page_label: bool,
    pub data: Vec<u8>,
}

impl<'a> Page<'a> {
    pub fn new(
        volume: usize,
        page: usize,
        settings: &'a Settings,
        cache_instance: &'a SvgCache,
    ) -> Page<'a> {
        Page {
            settings,
            cache_instance,
            found_in_cache: false,
            volume,
            page,
            show_page_label: page > 3 && page < 390,
            data: vec![],
        }
    }
    pub fn get_data_from_cache(&self) -> Option<Vec<u8>> {
        self.cache_instance.get_single_row(self.volume, self.page)
    }
    pub fn slowly_render_from_data(&mut self) {
        self.data = match self.page {
            1 | 2 | 3 => self.front_matter_as_vector_of_bytes(),
            390 | 391 => self.back_matter_as_vector_of_bytes(),
            _ => self.body_text_as_vector_of_bytes(),
        };
    }

    pub fn add_new_data_to_cache(&self) {
        self.cache_instance.put(self.volume, self.page, &self);
    }

    fn page_label(&self) -> String {
        match self.page {
            1 => "i".to_string(),
            2 => "ii".to_string(),
            3 => "iii".to_string(),
            390 => "iv".to_string(),
            391 => "v".to_string(),
            _ => format!(
                "{}",
                (self.volume - 1) * PAGINATED_PAGES_PER_VOLUME + self.page - 3
            )
            .separate_with_commas(),
        }
    }
    fn body_text_as_vector_of_bytes(&self) -> Vec<u8> {
        let path = format!("{}/pi-{}.txt", self.settings.data.path, self.volume);
        let mut f = File::open(path).unwrap();
        let mut buffer: [u8; CHARS_PER_PAGE] = [32; CHARS_PER_PAGE];
        let first_char = ((self.page - 3) - 1) * CHARS_PER_PAGE;
        f.seek(SeekFrom::Start(first_char as u64)).unwrap();
        let _bytes = f.read(&mut buffer[..]);

        let mut body: String;
        if self.page < 4 {
            body = "<text x=\"92\" y=\"75\">front matter".to_string();
        } else {
            body = format!(
                "<text x=\"92\" y=\"75\">{}",
                str::from_utf8(&buffer[0..CHARS_PER_LINE]).unwrap()
            );
            for i in 2..(LINES_PER_PAGE + 1) {
                let first_char = (i - 1) * CHARS_PER_LINE;
                let last_char = first_char + CHARS_PER_LINE;
                let string_in_loop = format!(
                    "<tspan x=\"92\" dy=\"1.451em\">{}</tspan>",
                    str::from_utf8(&buffer[first_char..last_char]).unwrap()
                );
                body.push_str(&string_in_loop);
            }
        }

        let body_text_footer_with_label = match self.show_page_label {
            true => format!(
                "</text><text text-anchor=\"middle\" x=\"325\" y=\"995\">{}</text></svg>",
                self.page_label()
            ),
            false => "</text></svg>".to_string(),
        };
        format!(
            "{}{}{}{}",
            SVG_COMMON_START_TAG, BODY_TEXT_HEADER, body, body_text_footer_with_label
        )
        .into_bytes()
    }
    #[allow(clippy::match_single_binding)]
    fn front_matter_as_vector_of_bytes(&self) -> Vec<u8> {
        match self.page {
            1 => format!(
                "{}{}{}{}",
                SVG_COMMON_START_TAG,
                PAGE_I_HEADER,
                self.volume.separate_with_commas(),
                PAGE_I_FOOTER,
            )
            .into_bytes(),
            2 => format!("{}{}", SVG_COMMON_START_TAG, PAGE_II,).into_bytes(),
            _ => format!("{}{}", SVG_COMMON_START_TAG, PAGE_III,).into_bytes(),
        }
    }
    #[allow(clippy::match_single_binding)]
    fn back_matter_as_vector_of_bytes(&self) -> Vec<u8> {
        match self.page {
            390 => format!("{}{}", SVG_COMMON_START_TAG, PAGE_IV,).into_bytes(),
            _ => format!("{}{}", SVG_COMMON_START_TAG, PAGE_V,).into_bytes(),
        }
    }
}

use config::Config;
use std::fs::OpenOptions;
use std::io::Write;
use std::net::{Ipv4Addr, SocketAddrV4};
use std::path::Path;
use std::path::PathBuf;

#[derive(Debug)]
pub struct Settings {
    pub server: ServerSettings,
    pub cache: CacheSettings,
    pub data: DataSettings,
}

#[derive(Debug)]
pub struct ServerSettings {
    pub ipv4: Ipv4Addr,
    pub port: u16,
}

#[derive(Debug)]
pub struct CacheSettings {
    pub in_memory: bool,
}

#[derive(Debug)]
pub struct DataSettings {
    pub path: String,
}

impl Settings {
    pub fn new() -> Settings {
        let mut settings_path = PathBuf::new();
        settings_path.push(dirs::home_dir().unwrap());
        settings_path.push("svgserver.settings.toml");
        if let false = Path::new(&settings_path).exists() {
            println!("No settings file found.");
            println!("Creating example file {}", settings_path.to_string_lossy());
            let mut file = OpenOptions::new()
                .write(true)
                .create(true)
                .open(&settings_path)
                .unwrap();
            let content = b"[server]\nipv4 = \"127.0.0.1\"\nport = \"7878\"\n\n[cache]\nin_memory = false\n\n[data]\npath = \"/mnt/milliontextfilesdisk/million\"\n";
            file.write_all(content).unwrap();
            println!("=========================================");
            println!("=========================================");
            println!("Make sure the generated svgserver.settings.toml file is correct.");
            println!("=========================================");
            println!("=========================================");
        }

        let mut settings = Config::default();
        settings
            .merge(config::File::from(settings_path.clone()))
            .unwrap();

        // Server
        let ipv4 = settings.get_str("server.ipv4").unwrap();
        let port: u16 = settings.get_int("server.port").unwrap() as u16;
        let server = ServerSettings {
            ipv4: ipv4.parse().unwrap(),
            port,
        };

        // Cache
        let in_memory = settings.get_bool("cache.in_memory").unwrap();
        let cache = CacheSettings { in_memory };

        // Data
        let data_path = settings.get_str("data.path").unwrap();
        let data = DataSettings { path: data_path };

        println!("Loading settings from {}", &settings_path.to_string_lossy());

        Settings {
            server,
            cache,
            data,
        }
    }
    pub fn socket(&self) -> SocketAddrV4 {
        SocketAddrV4::new(self.server.ipv4, self.server.port)
    }
}

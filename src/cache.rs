use super::settings::Settings;
use super::svg::Page;
use rusqlite::{params, Connection, Error, Result};
use std::path::Path;
use std::path::PathBuf;

#[derive(Debug)]
pub struct SvgCache {
    pub connection: Connection,
    pub server_version: String,
}

impl SvgCache {
    pub fn connect_or_create(settings: &Settings) -> SvgCache {
        SvgCache {
            connection: connect_according_to_settings(settings).unwrap(),
            server_version: env!("CARGO_PKG_VERSION").to_string(),
        }
    }
    pub fn get_single_row(&self, volume: usize, page: usize) -> Option<Vec<u8>> {
        let params = params![volume as u32, page as u32, self.server_version];
        let row: Result<Vec<u8>, Error> = self.connection.query_row(
            "SELECT data FROM svgfiles WHERE volume = ? AND page = ? AND server_version = ?",
            params,
            |row| row.get(0),
        );
        match row {
            Ok(data) => {
                self.connection.execute(
                    "UPDATE svgfiles SET last_touched = CURRENT_TIMESTAMP, times_touched = times_touched + 1 WHERE volume = ? AND page = ? AND server_version = ?",
                    params
                ).unwrap();
                Some(data)
            }
            Err(_) => None,
        }
    }
    pub fn put(&self, volume: usize, page: usize, svg: &Page) {
        let params = params![volume as u32, page as u32, svg.data, self.server_version];
        self.connection
            .execute(
                "INSERT INTO svgfiles (volume, page, data, server_version) VALUES (?1, ?2, ?3, ?4)",
                params,
            )
            .unwrap();
    }
}

fn connect_according_to_settings(settings: &Settings) -> Result<Connection> {
    let mut cache_path = PathBuf::new();
    cache_path.push(dirs::home_dir().unwrap());
    cache_path.push("svgserver.cache.sqlite");

    match Path::new(&cache_path).exists() {
        true => println!(
            "Attaching to database file {}",
            &cache_path.to_string_lossy()
        ),
        false => println!(
            "Creating new database file at {}",
            &cache_path.to_string_lossy()
        ),
    };
    const NEW_TABLE: &str = "CREATE TABLE svgfiles (
        id              INTEGER PRIMARY KEY,
        created         DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        last_touched    DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        times_touched   INTEGER DEFAULT 1 NOT NULL,
        volume          INTEGER DEFAULT 1 NOT NULL,
        page            INTEGER NOT NULL,
        server_version  TEXT NOT NULL,
        data          BLOB NOT NULL
    )";
    let connection = match &settings.cache.in_memory {
        true => Connection::open_in_memory().unwrap(),
        false => Connection::open(&cache_path).unwrap(),
    };
    match connection.execute(NEW_TABLE, params![]) {
        Ok(_) => println!("Creating new database table 'svgfiles'"),
        Err(_) => println!("Found existing database table 'svgfiles'"),
    };
    Ok(connection)
}

pub struct HttpResponse {
    pub data: Vec<u8>,
    pub status_code: usize,
}

impl HttpResponse {
    pub fn new() -> HttpResponse {
        let data = vec![];
        let status_code = 404;
        HttpResponse { data, status_code }
    }
    pub fn header(&self) -> Vec<u8> {
        match self.status_code {
            200 => format!(
                "HTTP/1.1 200 OK\r\nContent-Length: {}\r\nContent-Type: image/svg+xml\r\n\r\n",
                self.data.len()
            )
            .into_bytes(),
            404 => "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string().into_bytes(),
            _ => "HTTP/1.1 500 SERVER ERROR\r\n\r\n".to_string().into_bytes(),
        }
    }
    pub fn body(&self) -> Vec<u8> {
        match self.status_code {
            200 => self.data.clone(),
            _ => vec![],
        }
    }
}
